import { ChangeDetectionStrategy, Component } from "@angular/core";

export type MenuItems = {
  label: string;
  link: string;
};

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent {

  menuItems: MenuItems[] = [
    {
      label: "Trips",
      link: "/trips",
    },
    {
      label: "Contact us",
      link: "/contact-us",
    },
  ];

  trackByFn(_: number, item: MenuItems) {
    return item.link;
  }
}
