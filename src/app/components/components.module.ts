import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { UserLayoutComponent } from "./layouts/user-layout/user-layout.component";
import { NavigationComponent } from "./navigation/navigation.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
  ],
  declarations: [
    AdminLayoutComponent,
    UserLayoutComponent,
    NavigationComponent,
  ],
  exports: [
    AdminLayoutComponent,
    UserLayoutComponent,
    NavigationComponent,
  ],
})
export class ComponentsModule {

}
