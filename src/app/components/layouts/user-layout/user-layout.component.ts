import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserLayoutComponent {

}
