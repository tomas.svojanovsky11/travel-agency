import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { AbstractControl } from "@angular/forms";

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormErrorComponent {

  @Input() form: AbstractControl | null = null;

  @Input() fieldName = '';
}
