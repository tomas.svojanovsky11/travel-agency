import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomepageComponent } from "./modules/homepage/pages/homepage.component";
import { TripsComponent } from "./modules/trips/pages/trips/trips.component";
import { ContactUsComponent } from "./modules/contact-us/pages/contact-us.component";
import { LoginComponent } from "./modules/admin/pages/login/login.component";
import { UserLayoutComponent } from "./components/layouts/user-layout/user-layout.component";
import { AdminLayoutComponent } from "./components/layouts/admin-layout/admin-layout.component";
import { CreateTripComponent } from "./modules/admin/pages/create-trip/create-trip.component";
import { TripDetailComponent } from "./modules/trips/pages/detail/trip-detail.component";

const routes: Routes = [
  {
    path: '',
    component: UserLayoutComponent,
    children: [
      {
        path: '',
        component: HomepageComponent,
      },
      {
        path: 'trips',
        component: TripsComponent,
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
      },
      {
        path: 'trip/:id',
        component: TripDetailComponent,
      },
    ],
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'create-trip',
        component: CreateTripComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
