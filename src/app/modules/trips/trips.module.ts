import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { TripsComponent } from "./pages/trips/trips.component";
import { TripsService } from "./services/trips.service";
import { TripDetailComponent } from "./pages/detail/trip-detail.component";

@NgModule({
  imports: [HttpClientModule],
  declarations: [TripsComponent, TripDetailComponent],
  providers: [TripsService],
})
export class TripsModule {

}
