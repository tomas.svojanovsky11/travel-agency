import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-trip-detail',
  templateUrl: 'trip-detail.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TripDetailComponent {

}
