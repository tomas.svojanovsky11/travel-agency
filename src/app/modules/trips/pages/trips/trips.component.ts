import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TripsComponent {

}
