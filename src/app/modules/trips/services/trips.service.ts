import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export type Trip = {
  id: string;
  name: string;
}

@Injectable()
export class TripsService {

  constructor(private http: HttpClient) {
  }

  list(): Observable<Trip[]> {
    return this.http.get<Trip[]>('http://localhost:3000/trips')
  }
}
