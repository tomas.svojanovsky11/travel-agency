import { NgModule } from "@angular/core";

import { ContactUsComponent } from "./pages/contact-us.component";

@NgModule({
  declarations: [ContactUsComponent],
})
export class ContactUsModule {}
