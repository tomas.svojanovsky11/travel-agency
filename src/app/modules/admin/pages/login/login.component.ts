import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'app-admin-login',
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {

}
