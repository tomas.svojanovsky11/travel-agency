import { ChangeDetectionStrategy, Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { take } from "rxjs";

import { AuthService, LoginRequest } from "../../services/auth.service";

type FormValues = LoginRequest;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginFormComponent {

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
  ) {
  }

  form = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(1)]],
  });

  onSubmit(values: FormValues) {
    this.authService.login(values)
      .pipe(take(1))
      .subscribe()
  }
}
