import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

import { CreateTripValues, AdminTripService } from "../../services/admin-trip.service";
import { take } from "rxjs";

type FormValues = CreateTripValues;

@Component({
  selector: 'app-create-trip-component',
  templateUrl: 'create-trip.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateTripComponent {

  constructor(
    private formBuilder: FormBuilder,
    private tripService: AdminTripService,
  ) {
  }

  form = this.formBuilder.group({
    name: ['', [Validators.required]],
    departure: ['', [Validators.required]],
    return: ['', [Validators.required]],
    departureCity: ['', [Validators.required]],
    departureAirport: ['', [Validators.required]],
    destinationAirport: ['', [Validators.required]],
    destinationCity: ['', [Validators.required]],
    destinationHotel: ['', [Validators.required]],
    tripType: ['', [Validators.required]],
    priceAdults: [0, [Validators.required]],
    priceChildren: [0, [Validators.required]],
    bedsAdults: [0, [Validators.required]],
    bedsChildren: [0, [Validators.required]],
    promoted: [false, [Validators.required]],
  });

  tripTypes = [
    {
      value: "ROOM_ONLY",
    },
    {
      value: "BED_AND_BREAKFAST",
    },
    {
      value: "HALF_BOARD",
    },
    {
      value: "FULL_BOARD",
    },
    {
      value: "ALL_INCLUSIVE",
    },
  ];

  onSubmit(values: FormValues) {
    console.log(values);
    this.tripService.create(values)
      .pipe(take(1))
      .subscribe();
  }
}
