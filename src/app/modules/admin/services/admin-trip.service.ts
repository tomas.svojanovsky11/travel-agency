import { Injectable } from "@angular/core";
import { of } from "rxjs";

export type CreateTripValues = {
  name: string;
  departure: string;
  return: string;
  departureCity: string;
  departureAirport: string;
  destinationAirport: string;
  destinationCity: string;
  destinationHotel: string;
  tripType: string; // @TODO enum
  priceAdults: number;
  priceChildren: number;
  bedsAdults: number;
  bedsChildren: number;
  promoted: boolean;
};

@Injectable()
export class AdminTripService {

  create(values: CreateTripValues) {
    console.log(values);
    return of({});
  }

  list() {

  }
}
