import { Injectable } from '@angular/core';
import { of } from "rxjs";

export type LoginRequest = {
  email: string;
  password: string;
};

@Injectable()
export class AuthService {

  login(data: LoginRequest) {
    // @TODO call api
    console.log(data)
    return of([]);
  }
}
