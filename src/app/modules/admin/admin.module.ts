import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { LoginComponent } from "./pages/login/login.component";
import { LoginFormComponent } from "./pages/login/login-form.component";
import { AuthService } from "./services/auth.service";
import { SharedModule } from "../../shared/shared.module";
import { CreateTripComponent } from "./pages/create-trip/create-trip.component";
import { AdminTripService } from "./services/admin-trip.service";

@NgModule({
  imports: [
    ReactiveFormsModule,
    SharedModule,
    CommonModule,
  ],
  declarations: [
    LoginComponent,
    LoginFormComponent,
    CreateTripComponent,
  ],
  providers: [
    AuthService,
    AdminTripService,
  ],
})
export class AdminModule {
}
