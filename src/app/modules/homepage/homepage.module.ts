import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { HomepageComponent } from "./pages/homepage.component";
import { TripsModule } from "../trips/trips.module";
import { TripCardComponent } from "./components/trip-card/trip-card.component";

@NgModule({
  imports: [
    TripsModule,
    CommonModule,
    RouterModule
  ],
  declarations: [
    HomepageComponent,
    TripCardComponent,
  ],
})
export class HomepageModule {

}
