import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Trip } from "../../../trips/services/trips.service";

@Component({
  selector: 'app-trip-card',
  templateUrl: 'trip-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TripCardComponent {
  @Input() trip?: Trip;
}
