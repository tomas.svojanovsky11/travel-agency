import { ChangeDetectionStrategy, Component } from "@angular/core";

import { TripsService } from "../../trips/services/trips.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomepageComponent {

  constructor(private tripsService: TripsService) {
  }

  trips = this.tripsService.list();
}
