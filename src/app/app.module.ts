import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ContactUsModule } from "./modules/contact-us/contact-us.module";
import { TripsModule } from "./modules/trips/trips.module";
import { HomepageModule } from "./modules/homepage/homepage.module";
import { AdminModule } from "./modules/admin/admin.module";
import { ComponentsModule } from "./components/components.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    ComponentsModule,
    ContactUsModule,
    TripsModule,
    HomepageModule,
    AdminModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
